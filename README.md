公用的 Shell 脚本
======

### 项目说明
这是一个公用 Shell 脚本的项目仓库。  

### 开发说明
- 代码尽量相互隔离，减少相关性
- 代码请标注完整的使用案例

### 为项目做贡献
本项目是否一个开源项目，欢迎任何人为其开发和进步贡献力量。
- 在使用过程中出现任何问题，请通过 [Issue](https://github.com/ztj1993/CommonShell/issues) 反馈
- Bug 修复可以直接 Pull Request
- 如果要联系作者，请发送邮件至 ztj1993@gmail.com
